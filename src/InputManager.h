#pragma once
//#include "ofMain.h"
//NOW DEPRECATED. USE ofGetKeyPressed() INSTEAD!
class InputManager {
private:
	bool InputState[512] = { false }; //512 seems like a pretty good sized allocation for every theoretical keyboard/joypad scancode that could exist.... right?
	bool MouseState[16] = { false };
public:
	//ofVec2f mousePosition;

	void handleInputEnter(int key);
	void handleInputExit(int key);
	void handleMousePress(int button);
	void handleMouseDepress(int button);

	bool keyIsPressed(int key);
	bool mouseIsPressed(int button);
};
