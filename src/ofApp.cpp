#include "ofApp.h"

// OCTREE METHODS WRITTEN BY SEAN CHAN, IMPLEMENTED BY DAVID NAVARRO
//---------------------OCTREE METHODS---------------------------

void ofApp::indexVertices(Box & leaf, vector <int> & supersetIndices, ofMesh & mesh) {
	for (int i = 0; i < supersetIndices.size(); ++i) {
		ofVec3f point = mesh.getVertex(supersetIndices[i]);
		if (leaf.contains(Vector3(point.x, point.y, point.z))) {
			leaf.meshIndices.push_back(supersetIndices[i]);
		}
	}
}
void ofApp::getTraceBoxes(Box & searchBox, Ray & searchRay, vector <Box> & collection, int maxLevel, int currentLevel) {
	int tolerance = 256;
	if (searchBox.intersect(searchRay, -tolerance, tolerance)) {
		if (currentLevel >= maxLevel && searchBox.meshIndices.size() > 0) {
			collection.push_back(searchBox);
			return;
		}
		for (int i = 0; i < searchBox.children.size(); ++i) {
			getTraceBoxes(searchBox.children[i], searchRay, collection, maxLevel, currentLevel + 1);
		}
		//All boxes that intersect with the ray will be collected into the vector fed into the function.
	}
}
ofVec3f ofApp::getPointFromTrace(Box & searchBox, Ray & raytrace, ofMesh & mesh, int maxLevel, int currentLevel) {
	float timeExecuted = ofGetElapsedTimeMillis();
	vector<Box> traceBoxes;
	getTraceBoxes(searchBox, raytrace, traceBoxes, maxLevel, currentLevel);
	const ofVec3f origin = ofVec3f(raytrace.origin.x(), raytrace.origin.y(), raytrace.origin.z());
	ofVec3f & point = origin + ofVec3f(raytrace.direction.x(), raytrace.direction.y(), raytrace.direction.z()) * 9001;
	
	for (int i = 0; i < traceBoxes.size(); ++i) {
		if (traceBoxes[i].meshIndices.size() < 1) { continue; }
		ofVec3f result;
		if (!doTargetedPointSelection(traceBoxes[i], result, raytrace, mesh)) { continue; } //If the point selection routine reports that it did not hit, we skip the next part and continue looping.
		if (result.distanceSquared(origin) < point.distanceSquared(origin)) {
			point = result;
		}
	}
	//cout << ofGetElapsedTimeMillis() - timeExecuted; cout << " milliseconds to find point.\n";
	return point;
}
void ofApp::recursiveBoxDraw(Box *targetBox, int maxLevel, int currentLevel) {

	if (currentLevel > maxLevel) {
		return;
	}
	switch (currentLevel) {
	case 0: ofSetColor(ofColor::red, 250); drawBox(*targetBox); break;
	case 1: ofSetColor(ofColor::orange, 200); drawBox(*targetBox); break;
	case 2: ofSetColor(ofColor::yellow, 150); drawBox(*targetBox); break;
	case 3: ofSetColor(ofColor::green, 100); drawBox(*targetBox); break;
	case 4: ofSetColor(ofColor::blue, 70); drawBox(*targetBox); break;
	default: ofSetColor(ofColor::white, 55); drawBox(*targetBox);
	}
	for (int i = 0; i < targetBox->children.size(); ++i) {
		recursiveBoxDraw(&targetBox->children[i], maxLevel, currentLevel + 1);
	}
}
void ofApp::populateLeaves(Box & root, int level, int maxLevel, ofMesh & mesh) {
	if (level < 1) {
		int size = terrain.getMesh(0).getVertices().size(); // so I dont have to wait forever
		cout << "initial superset population\n";
		cout << terrain.getMesh(0).getNumVertices(); cout << " vertices in the mesh.\n";
		for (int i = 0; i < size; ++i) {
			root.meshIndices.push_back(i);
		}
	}
	if (level >= maxLevel) {
		return;
	}
	subDivideBox8(root, root.children);
	for (int i = 0; i < root.children.size(); ++i) {
		indexVertices(root.children[i], root.meshIndices, mesh);
		populateLeaves(root.children[i], level + 1, maxLevel, mesh);
	}
}

bool ofApp::doTargetedPointSelection(Box & searchBox, ofVec3f & resultPoint, Ray & raytrace, ofMesh & mesh) {
	int n = searchBox.meshIndices.size();
	float nearestDistance = 0;
	int nearestIndex = 0;

	bool bPointSelected = false;

	ofVec3f rayOrigin = ofVec3f(raytrace.origin.x(), raytrace.origin.y(), raytrace.origin.z());
	ofVec3f rayDirection = ofVec3f(raytrace.direction.x(), raytrace.direction.y(), raytrace.direction.z());
	vector<ofVec3f> selection;

	// We check through the mesh vertices to see which ones
	// are "close" to the ray.  If we find points that are close, we 
	// store them in a vector (dynamic array)
	//
	for (int i = 0; i < n; i++) {
		ofVec3f vert = mesh.getVertex(searchBox.meshIndices[i]);
		ofVec3f vertdir = (rayOrigin - vert).normalize();
		float dot = vertdir.dot(rayDirection);
		if (dot < selectionRange) {
			selection.push_back(vert);
			bPointSelected = true;
		}
	}

	//  if we found selected points, we need to determine which
	//  one is closest to the eye (camera). That one is our selected target.
	//
	if (bPointSelected) {
		float distance = 0;
		for (int i = 0; i < selection.size(); i++) {
			ofVec3f point = selection[i];

			// In camera space, the camera is at (0,0,0), so distance from 
			// the camera is simply the length of the point vector
			//
			float curDist = rayOrigin.distance(point);

			if (i == 0 || curDist < distance) {
				distance = curDist;
				resultPoint = selection[i];
			}
		}
	}
	return bPointSelected;
}

bool ofApp::doTargetedPointSelection(Box & searchBox, ofVec3f & resultPoint, ofMesh & mesh) {
	int n = searchBox.meshIndices.size();
	float nearestDistance = 0;
	int nearestIndex = 0;

	bool bPointSelected = false;

	ofVec2f mouse(mouseX, mouseY);
	vector<ofVec3f> selection;

	// We check through the mesh vertices to see which ones
	// are "close" to the mouse point in screen space.  If we find 
	// points that are close, we store them in a vector (dynamic array)
	//
	for (int i = 0; i < n; i++) {
		ofVec3f vert = mesh.getVertex(searchBox.meshIndices[i]);
		ofVec3f posScreen = cameraManager.focusCam->worldToScreen(vert);
		float distance = posScreen.distance(mouse);
		if (distance < selectionRange) {
			selection.push_back(vert);
			bPointSelected = true;
		}
	}

	//  if we found selected points, we need to determine which
	//  one is closest to the eye (camera). That one is our selected target.
	//
	if (bPointSelected) {
		float distance = 0;
		for (int i = 0; i < selection.size(); i++) {
			ofVec3f point = cameraManager.focusCam->worldToCamera(selection[i]);

			// In camera space, the camera is at (0,0,0), so distance from 
			// the camera is simply the length of the point vector
			//
			float curDist = point.length();

			if (i == 0 || curDist < distance) {
				distance = curDist;				
				resultPoint = selection[i];
			}
		}
	}
	return bPointSelected;
}

//draw a box from a "Box" class  
//
void ofApp::drawBox(const Box &box) {
	Vector3 min = box.parameters[0];
	Vector3 max = box.parameters[1];
	Vector3 size = max - min;
	Vector3 center = size / 2 + min;
	ofVec3f p = ofVec3f(center.x(), center.y(), center.z());
	float w = size.x();
	float h = size.y();
	float d = size.z();
	ofDrawBox(p, w, h, d);
}

// return a Mesh Bounding Box for the entire Mesh
//
Box ofApp::meshBounds(const ofMesh & mesh) {
	int n = mesh.getNumVertices();
	ofVec3f v = mesh.getVertex(0);
	ofVec3f max = v;
	ofVec3f min = v;
	for (int i = 1; i < n; i++) {
		ofVec3f v = mesh.getVertex(i);

		if (v.x > max.x) max.x = v.x;
		else if (v.x < min.x) min.x = v.x;

		if (v.y > max.y) max.y = v.y;
		else if (v.y < min.y) min.y = v.y;

		if (v.z > max.z) max.z = v.z;
		else if (v.z < min.z) min.z = v.z;
	}
	return Box(Vector3(min.x, min.y, min.z), Vector3(max.x, max.y, max.z));
}

//  Subdivide a Box into eight(8) equal size boxes, return them in boxList;
//
void ofApp::subDivideBox8(const Box &box, vector<Box> & boxList) {
	Vector3 min = box.parameters[0];
	Vector3 max = box.parameters[1];
	Vector3 size = max - min;
	Vector3 center = size / 2 + min;
	float xdist = (max.x() - min.x()) / 2;
	float ydist = (max.y() - min.y()) / 2;
	float zdist = (max.z() - min.z()) / 2;
	Vector3 h = Vector3(0, ydist, 0);

	//  generate ground floor
	//
	Box b[8];
	b[0] = Box(min, center);
	b[1] = Box(b[0].min() + Vector3(xdist, 0, 0), b[0].max() + Vector3(xdist, 0, 0));
	b[2] = Box(b[1].min() + Vector3(0, 0, zdist), b[1].max() + Vector3(0, 0, zdist));
	b[3] = Box(b[2].min() + Vector3(-xdist, 0, 0), b[2].max() + Vector3(-xdist, 0, 0));

	boxList.clear();
	for (int i = 0; i < 4; i++)
		boxList.push_back(b[i]);

	// generate second story
	//
	for (int i = 4; i < 8; i++) {
		b[i] = Box(b[i - 4].min() + h, b[i - 4].max() + h);
		boxList.push_back(b[i]);
	}
}
//---------------------END OCTREE METHODS-----------------------

//--------------------------------------------------------------
void ofApp::setup(){
	//
	// set up camera stuff
	//
	cameraManager.setup();
	cameraCounter = 0;
	focus.set(0, 0, 0);
	ofEnableSmoothing();
	ofEnableDepthTest();
	bckgnd.loadImage("images/starField.jpg");
	terrain.loadModel("geo/moon-houdini.obj");
	terrain.setScaleNormalization(false);

	boundingBox = meshBounds(terrain.getMesh(0));
	terrainMesh = terrain.getMesh(0);

	initLightingAndMaterials();
	ofSetVerticalSync(true);
	ofSetFrameRate(60);

	// texture loading
	//
	ofDisableArbTex();     // disable rectangular textures

	// load textures
	//
	if (!ofLoadImage(particleTex, "images/dot.png")) {
		cout << "Particle Texture File: images/dot.png not found" << endl;
		ofExit();
	}

	// load the shader
	//
#ifdef TARGET_OPENGLES
	shader.load("shaders_gles/shader");
#else
	shader.load("shaders/shader");
#endif
	
	// AUTHOR: DAVID NAVARRO
	// setup your objects here

	Particle rocketBall;
	rocketBall.radius = 0.5;
	rocketBall.lifespan = 100000;
	rocketBall.position = ofVec3f(150, 50, 150);

	sysLander.add(rocketBall);
	landerPos = sysLander.particles[0].position;
	landerRayDir = ofVec3f(0, -1, 0);
	octreeSelectedPoint = ofVec3f(0, 0, 0);

	thrustVector = ofVec3f(0, 0, 0);
	thrustForce = new ThrustForce(ofVec3f(0, 0, 0));
	impulseForce = new ImpulseForce();
	gravityForce = new GravityForce(ofVec3f(0, -0.162, 0));
	//turbulenceForce = new TurbulenceForce(ofVec3f(-0.2, -0.1, 0.3), ofVec3f(0.1, 0.1, 0.2));
	sysLander.addForce(thrustForce);
	sysLander.addForce(impulseForce);
	sysLander.addForce(gravityForce);
	//sysLander.addForce(turbulenceForce);

	
	thrustEmitter1 = new ParticleEmitter();
	thrustEmitter1->setEmitterType(HaloEmitter);
	thrustEmitter1->setGroupSize(10);
	thrustEmitter1->setLifespan(1);
	thrustEmitter1->setLifespanRange(ofVec2f(0.25, 0.75));
	thrustEmitter1->setRandomLife(true);
	thrustEmitter1->setParticleRadius(0.05);
	thrustEmitter1->setRadius(1);
	thrustEmitter1->setRate(0);
	thrustEmitter1->setVelocity(ofVec3f(0, 0, 0));
	thrustEmitter1->start();

	explosionEmitter = new ParticleEmitter();
	explosionEmitter->setEmitterType(RadialEmitter);
	explosionEmitter->setGroupSize(50);
	explosionEmitter->setOneShot(true);
	explosionEmitter->setLifespan(1);
	explosionEmitter->setLifespanRange(ofVec2f(1, 3));
	explosionEmitter->setRandomLife(true);
	explosionEmitter->setParticleRadius(0.1);
	explosionEmitter->setRadius(1);
	explosionEmitter->setRate(0);
	explosionEmitter->setVelocity(ofVec3f(3, 3, 3));

	landerSound.load("sounds/rocket.wav");

	starFoxArwing = Entity("geo/ArwingHiPoly.obj", ofVec3f(.05) );
	starFoxArwing.boundingBoxDimensions = ofVec3f(4, 2, 4);
	//starFoxArwing.pos = ofVec3f(150, 50, 150);
	starFoxArwing.setAngle(ofVec3f(0, 240, 0));
	cockPit = Entity("geo/ArwingCockpit.obj", ofVec3f(.02) );
	cameraManager.cameras[3].setFov(60);
	cameraManager.cameras[3].setNearClip(1);

	//playerMissile.expireTime = ofGetElapsedTimef() + 1000;
	//playerMissile.pos += ofVec3f(0, 10, 0);


	cout << "Loading Terrain Octree..." << endl;
	bPointSelected = false;
	bCollision = false;
	float creationTime = ofGetElapsedTimeMillis();
	populateLeaves(boundingBox, 0, 5, terrainMesh);
	cout << "Time to create octree = "; cout << (ofGetElapsedTimeMillis() - creationTime); cout << " ms.\n";
	
	cout << "Setting scenery.\n";
	setupScene();
}

//--------------------------------------------------------------
void ofApp::update(){
	//adjust background
	if (isFullScreen) {
		bckgnd.resize(ofGetScreenWidth(), ofGetScreenHeight());
	}
	else {
		bckgnd.resize(ofGetWindowWidth(), ofGetWindowHeight());	
	}

	// Movement Check
	landerMovement();

	// udpate your objects here
	landerPos = sysLander.particles[0].position;
	
	if (cameraCounter == 3) {
		starFoxArwing.shouldDraw = false;
		cockPit.shouldDraw = true;
	}
	else { starFoxArwing.shouldDraw = true; cockPit.shouldDraw = false; }
	if (cameraCounter == 2) {
		cameraManager.cameras[2].setOrientation(ofVec3f(90, starFoxArwing.angYaw, 0));
	}
	//update Camera ----
	if (cameraCounter < 5) {
		cameraManager.setPosition(landerPos, cameraCounter);
	}
	else {
	//	cameraManager.setPosition(mousePos, cameraCounter);
	}

	// Check Collisions -----
	checkLanderTerrainCollision();

	thrustEmitter1->setPosition(landerPos + ofVec3f(0, -2, 0));
	thrustEmitter1->update();
	explosionEmitter->setPosition(landerPos); // Change vector according to where you want explosion
	explosionEmitter->update();
	sysLander.update();

	
	starFoxArwing.pos = landerPos;
	cockPit.pos = landerPos; cockPit.setAngle(starFoxArwing.getAngle());
	starFoxArwing.update(); cockPit.update();
	ofVec3f cockpitOffset = ofVec3f(0,1.6,0);
	cameraManager.cameras[3].setOrientation(starFoxArwing.getAngle() + ofVec3f(0,180,0));
	cameraManager.cameras[3].setPosition(landerPos + -5 * cockPit.getDir() + cockpitOffset);
	
	//cameraManager.cameras[6].setOrientation(playerMissile.getAngle() + ofVec3f(0, 180, 0));
	//cameraManager.cameras[6].setPosition(playerMissile.pos + playerMissile.getDir() * -1 + ofVec3f(0, 1, 0));

	//Entities
	for (int i = 0; i < entContainer.size(); ++i) {
		Entity * currentEnt = entContainer[i];
		currentEnt->update();
	}
}
static float ambientPolyMode[] =
{ 1, 1, 1, 1.0f };
//--------------------------------------------------------------
void ofApp::draw(){
	ofDisableDepthTest();
	ofSetColor(255, 255, 255);
	bckgnd.draw(0, 0);
	if (cameraCounter == 3) {
		ofSetColor(ofColor::white);
		std::ostringstream agl;
		agl << "AGL : " << octreeSelectedPoint.distance(landerPos);
		string s(agl.str());
		ofDrawBitmapString(s, 75, 75);
	}
	ofEnableDepthTest();
	//3D CONTEXT START
	cameraManager.begin();
	ofPushMatrix();

	ofEnableLighting();
	terrain.drawFaces();

	initLightingAndMaterialsFlat();


	starFoxArwing.draw();
	cockPit.draw();
	for (int i = 0; i < entContainer.size(); ++i) {
		entContainer[i]->draw();
	}
	initLightingAndMaterials();


	ofPopMatrix();
	cameraManager.end();
	//3D CONTEXT END

	// Particle Shader Start
	loadVbo();
	glDepthMask(GL_FALSE);

	ofSetColor(255, 100, 90);

	// this makes everything look glowy
	//
	ofEnableBlendMode(OF_BLENDMODE_ADD);
	ofEnablePointSprites();


	// begin drawing in the camera
	//
	shader.begin();
	cameraManager.begin();

	// draw particle emitter here..
	//
	particleTex.bind();
	vbo.draw(GL_POINTS, 0, (int)(thrustEmitter1->sys->particles.size() + explosionEmitter->sys->particles.size()));
	particleTex.unbind();

	//  end drawing in the camera
	// 
	cameraManager.end();
	shader.end();

	ofDisablePointSprites();
	ofDisableBlendMode();
	ofEnableAlphaBlending();

	// set back the depth mask
	//
	glDepthMask(GL_TRUE);

}

// AUTHOR: DAVID NAVARRO
//----------------LANDER PROJECT METHODS------------------------
void ofApp::landerMovement() {
	
	// Forward and Backward Movement
	if (ofGetKeyPressed(OF_KEY_UP)) {
		thrustVector = starFoxArwing.getDir() * 2;
		thrustForce->set(thrustVector);
	}
	else if (ofGetKeyPressed(OF_KEY_DOWN)) {
		thrustVector = -starFoxArwing.getDir() * 2;
		thrustForce->set(thrustVector);
	}

	// Left and Right Turning
	if (ofGetKeyPressed(OF_KEY_LEFT)) {
		starFoxArwing.angYaw += 0.75;
	}
	else if (ofGetKeyPressed(OF_KEY_RIGHT)) {
		starFoxArwing.angYaw += -0.75;
	}
	
	// Up and Down
	if (ofGetKeyPressed(' ')) {
		thrustVector = ofVec3f(0, 0.5, 0);
		thrustForce->set(thrustVector);
		thrustEmitter1->setVelocity(ofVec3f(0, -5, 0));
		thrustEmitter1->setRate(10);
		playLanderSound();
	}
	else if (ofGetKeyPressed(OF_KEY_ALT) && !bCollision) {
		thrustVector = ofVec3f(0, -0.5, 0);
		thrustForce->set(thrustVector);
		thrustEmitter1->setVelocity(ofVec3f(0, 5, 0));
		thrustEmitter1->setRate(10);
		playLanderSound();
	}

	// Turn of emitter
	if (!ofGetKeyPressed(' ') && !ofGetKeyPressed(OF_KEY_ALT))
		thrustEmitter1->setRate(0);
	
	
}

void ofApp::playLanderSound() {
	if (!landerSound.isPlaying()) {
		landerSound.play();
		cout << "Playing sound..." << endl;
	}
}

// load vertex buffer in preparation for rendering
//
void ofApp::loadVbo() {
	if (thrustEmitter1->sys->particles.size() < 1 && explosionEmitter->sys->particles.size() < 1) return;

	vector<ofVec3f> sizes;
	vector<ofVec3f> points;
	for (int i = 0; i < thrustEmitter1->sys->particles.size(); i++) {
		points.push_back(thrustEmitter1->sys->particles[i].position);
		sizes.push_back(ofVec3f(10));
	}

	for (int i = 0; i < explosionEmitter->sys->particles.size(); i++) {
		points.push_back(explosionEmitter->sys->particles[i].position);
		sizes.push_back(ofVec3f(50));
	}
	// upload the data to the vbo
	//
	int total = (int)points.size();
	vbo.clear();
	vbo.setVertexData(&points[0], total, GL_STATIC_DRAW);
	vbo.setNormalData(&sizes[0], total, GL_STATIC_DRAW);
}

void ofApp::checkLanderTerrainCollision() {
		
	landerRayDir.normalize();
	landerRay = Ray(Vector3(landerPos.x, landerPos.y, landerPos.z),	Vector3(landerRayDir.x, landerRayDir.y, landerRayDir.z));

	octreeSelectedPoint = getPointFromTrace(boundingBox, landerRay, terrainMesh, 5, 0);
	bPointSelected = boundingBox.contains(Vector3(octreeSelectedPoint.x, octreeSelectedPoint.y, octreeSelectedPoint.z));
	
	cout << "Why is this..." << bPointSelected << endl;

	if (octreeSelectedPoint.distance(landerPos) < collisionTerrainDist) {
		bCollision = true;
		ofVec3f normalForce = ofVec3f(0, 1, 0);
		ofVec3f particleVelocity = sysLander.particles[0].velocity;
		ofVec3f resultForce = (restitution + 1.0) * (-(particleVelocity.dot(normalForce)) * normalForce);
		impulseForce->apply(ofGetFrameRate() * resultForce);
		impulseForce->apply(ofVec3f(0, 0.5, 0));
		cout << "COLLISION DETECTED " << endl;
	}
	else {
		bCollision = false;
	}

	if (bCollision)
		sysLander.particles[0].velocity = ofVec3f(0, 0, 0);

	// Visual Ray for Debugging
	if (bPointSelected) {
		ofSetColor(ofColor::greenYellow);
		ofDrawLine(landerPos, octreeSelectedPoint);
		ofDrawSphere(octreeSelectedPoint, 0.1);
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (!inputs.keyIsPressed(key)) {
		inputs.handleInputEnter(key);
	}

	switch (key) {
	case '0':
		cameraCounter = 0;
		break;
	case '1':
		cameraCounter = 1;
		break;
	case '2' :
		cameraCounter = 2;
		break;
	case '3' :
		cameraCounter = 3;
		break;
	case '4':
		cameraCounter = 4;
		break;
	case '5':
		cameraCounter = 5;
		break;
	case '6':
		cameraCounter = 6;
		break;
	case 'F':
	case 'f':
		isFullScreen = !isFullScreen;
		ofToggleFullscreen();
		break;
	case 'R': //Press R to reset the lander position (debug feature)
	case 'r':
		/*sysLander.particles[0].position = ofVec3f(0, 4, 0);
		thrustForce->set(ofVec3f(0, 0, 0));
		thrustVector = ofVec3f(0, 0, 0);*/
		break;
	case 'E': //Press E to test explosion emitter
	case 'e':
		/*explosionEmitter->sys->reset();
		explosionEmitter->start();*/
		break;
	case ' ':
		break;
	case OF_KEY_ALT:
		break;
	case OF_KEY_UP:
		break;
	case OF_KEY_DOWN:
		break;
	case OF_KEY_LEFT:
		break;
	case OF_KEY_RIGHT:
		break;
	case OF_KEY_TAB:
		cameraCounter++;
		if (cameraCounter > cameraManager.cameras.size() - 1) {
			cameraCounter = 0;
		}
		cameraManager.switchCamera(cameraCounter);
		break;
	case 'h':
		if (cameraCounter == 5) {
			toggleFocus = !toggleFocus;
			if (toggleFocus) {
				cameraManager.cameras[5].setTarget(landerPos);
			}
			else {
				cameraManager.cameras[5].reset();
				cameraManager.setPosition(ofVec3f(-32, 9, 30.5), cameraCounter);
				cameraManager.cameras[5].setTarget(ofVec3f(0, 0, 10));
			}
		}
		break;
	}

	cameraManager.switchCamera(cameraCounter);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	if (inputs.keyIsPressed(key)) {
		inputs.handleInputExit(key);
	}
	switch (key) {
	case 'F':
	case 'f':
		break;
	case ' ':
	case OF_KEY_ALT:
		thrustEmitter1->setRate(0);
		break;
	case OF_KEY_UP:
	case OF_KEY_DOWN:
	case OF_KEY_LEFT:
	case OF_KEY_RIGHT:
		break;
	case 'h':
		break;
	}
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

//	cout << "mouse pressed \n";
	ofVec3f mousePoint = cameraManager.focusCam->screenToWorld(ofVec3f(x,y));
	ofVec3f rayDir = mousePoint - cameraManager.focusCam->getPosition();
	rayDir.normalize();
	Ray Mouseray = Ray(Vector3(mousePoint.x, mousePoint.y, mousePoint.z),
		Vector3(rayDir.x, rayDir.y, rayDir.z));

	/*
	playerMissile = Missile(true);
	playerMissile.pos = starFoxArwing.pos;
	playerMissile.setAngle(starFoxArwing.getAngle());
	/*
	switch (button) {
	case GLFW_MOUSE_BUTTON_3:
		mousePos= getPointFromTrace(boundingBox, Mouseray, terrainMesh, 5,0);
	//	cout << mousePos << " Mouse position \n";
		mousePosSelected = boundingBox.contains(Vector3(mousePos.x, mousePos.y, mousePos.z));
		if (mousePosSelected&&cameraCounter == 5) {
			cameraManager.setPosition(mousePos, cameraCounter);
			toggleFocus = false;
		}
		break;
	}
	
	inputs.handleMousePress(button);
	ofVec3f result;
	ofVec3f rayend = cameraManager.focusCam->screenToWorld(ofVec2f(x,y)); ofVec3f rayOrig = cameraManager.focusCam->getPosition();
	ofVec3f raydir = (rayend - rayOrig).normalize();
	result = getPointFromTrace(boundingBox, Ray(Vector3(rayOrig.x, rayOrig.y, rayOrig.z), Vector3(raydir.x, raydir.y, raydir.z)), terrainMesh, 5, 0);
	//cout << result << " from click trace\n";
	/**/

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
	inputs.handleMouseDepress(button);
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//Lighting Init
void ofApp::initLightingAndMaterials() {

	static float ambient[] =
	{ .5f, .5f, .5, 1.0f };
	static float diffuse[] =
	{ 1.0f, 1.0f, 1.0f, 1.0f };

	static float position[] =
	{ 5.0, 5.0, 5.0, 0.0 };

	static float lmodel_ambient[] =
	{ 1.0f, 1.0f, 1.0f, 1.0f };

	static float lmodel_twoside[] =
	{ GL_TRUE };


	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, position);

	glLightfv(GL_LIGHT1, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT1, GL_POSITION, position);


	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE, lmodel_twoside);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glDisable(GL_LIGHT1);
	glShadeModel(GL_SMOOTH);
}
void ofApp::initLightingAndMaterialsFlat() {

	static float ambient[] =
	{ 1, 1, 1, 1.0f };
	static float diffuse[] =
	{ 1.0f, 1.0f, 1.0f, 1.0f };

	static float position[] =
	{ 5.0, 5.0, 5.0, 0.0 };
	static float position2[] =
	{ -5.0, -5.0, -5.0, 0.0 };

	static float lmodel_ambient[] =
	{ 1.0f, 1.0f, 1.0f, 1.0f };

	static float lmodel_twoside[] =
	{ GL_TRUE };


	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, position);

	glLightfv(GL_LIGHT1, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT1, GL_POSITION, position2);


	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE, lmodel_twoside);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glShadeModel(GL_FLAT);
}
void ofApp::setupScene() {
	
	Entity * sceneObject;

	//OrbitalMissile
	
	sceneObject = new Entity("geo/Missile.obj", ofVec3f(.06, .06, .06));
	
	sceneObject->pos = ofVec3f(0, 10, -20);
	sceneObject->setAngle(ofVec3f(-90,0,0));
	sceneObject->boundingBoxDimensions = ofVec3f(5, 5, 5);
	entContainer.push_back(sceneObject);

	/*
	sceneObject = new Missile(true);
	sceneObject->pos = ofVec3f(0, 0, 0);
	entContainer.push_back(sceneObject);
	/**/
	
	//Crystals
	/*
	for (int i = 0; i < 5; ++i) {
		sceneObject = Entity("geo/DiagonalCrystal_Embedded.obj", ofVec3f(.025,.025,.025));
		sceneObject.boundingBoxDimensions = ofVec3f(5, 5, 5);
		sceneObject.angYaw = 120 * i;
		

		switch (i) {
		case 0:
			sceneObject.pos = ofVec3f(-60, 3.6, .55);
			break;
		case 1:
			sceneObject.pos = ofVec3f(-31.5, 1.4, 29.5);
			break;
		case 2:
			sceneObject.pos = ofVec3f(-20, -1, 17);
			break;
		case 3:
			sceneObject.pos = ofVec3f(-15.5, -1, 50);
			break;
		case 4:
			sceneObject.pos = ofVec3f(-40, 3.4, -4.4);
		}
		if (i % 2 == 0) {
			Entity vertCrystal = Entity("geo/VerticalCrystal_Embedded.obj", ofVec3f(.035, .08, .035));
			vertCrystal.pos = sceneObject.pos + sceneObject.getDir() * -5;
			entContainer.push_back(vertCrystal);
		}

		entContainer.push_back(sceneObject);
	}
	/**/

	//Main base building
	sceneObject = new Entity("geo/CargoShip.obj", ofVec3f(.15, .2, .15));
	sceneObject->pos = ofVec3f(50, -2, 50);
	sceneObject->angYaw = 180;
	entContainer.push_back(sceneObject);

	//Other base buildings
	sceneObject = new Entity("geo/VerticalCrystal_Embedded.obj", ofVec3f(.1, .042, .1));
	sceneObject->pos = ofVec3f(40, -1, -10);
	entContainer.push_back(sceneObject);
	
	sceneObject = new Entity("geo/SpaceTruck.obj", ofVec3f(.1, .15, .1));
	sceneObject->pos = ofVec3f(25, -1, 50);
	sceneObject->angYaw = -90;
	entContainer.push_back(sceneObject);


	//Turrets
	sceneObject = new Entity("geo/Turret.obj", ofVec3f(.025, .025, .025));
	sceneObject->color = ofColor::darkBlue;
	sceneObject->pos = ofVec3f(20, -2, -20); sceneObject->angYaw = 120;
	entContainer.push_back(sceneObject);
	sceneObject = new Entity("geo/Turret.obj", ofVec3f(.025, .025, .025));
	sceneObject->pos = ofVec3f(-20, -1, -20); sceneObject->angYaw = -120;
	entContainer.push_back(sceneObject);
	sceneObject = new Entity("geo/Turret.obj", ofVec3f(.025, .025, .025));
	sceneObject->pos = ofVec3f(0, -1, 20); sceneObject->angYaw = 0;
	entContainer.push_back(sceneObject);
}