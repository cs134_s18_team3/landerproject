#include "Cameras.h"
//Code Written by Jason Liu
Cameras::Cameras()
{

}
void Cameras::setPosition(ofVec3f p, int k) {
	switch (k) {
	case 0: //stationary position
		cameras[0].setPosition(ofVec3f(20, 20, 20));
		cameras[0].setTarget(p);
		break;
	case 1: //View of Side exterior of craft || View distance can be set by adjusting Z value 
		cameras[1].setAutoDistance(false);
		cameras[1].setPosition(p+ofVec3f(0,0,25));
		cameras[1].setTarget(p);
		break;
	case 2:	//View of terrain from bottom of craft
		cameras[2].setTarget(ofVec3f(p.x,0,p.z));
		cameras[2].setPosition(p+ofVec3f(0, -.5, 0)); //-y offset enough so that view is not disrupted by object.
		break;
	case 3:	//View of terrain from bottom of craft
		break;
	case 4:	//similar to camera 0 except this cam allows mouse inputs
		cameras[4].setTarget(p);
		break;
	case 5:	//Easy Cam position set to point on terrain
		//cameras[5].setTarget(ofVec3f(0, 5, 0));
		cameras[5].setPosition(ofVec3f(p.x,10,p.z));
		break;
	}
}
void Cameras::setup() {

	for (ofEasyCam cam : cameras) {
		cam.setDistance(50);
		cam.setNearClip(0.01);
		cam.setFov(65.5);
	}
	cameras[4].setDistance(30);
	cameras[4].setPosition(ofVec3f(0, 10, 25));
	cameras[4].enableMouseInput();
	cameras[5].setPosition(ofVec3f(-32, 9, 30.5));
	cameras[5].setTarget(ofVec3f(0, 10, 0));
	cameras[5].setAutoDistance(false);
	cameras[5].enableMouseInput();
	focusCam = &cameras[0];
}
void Cameras::begin() {
	focusCam->begin();
}
void Cameras::end() {
	focusCam->end();
}
void Cameras::switchCamera(int key) {
	switch (key) {
	case 0:
		focusCam = &cameras[0];
		//cout << "Camera 0 Stationary Camera \n";
		//cout << "camera 0 /n"<<cameras[0].getDistance();
		break;
	case 1:
		focusCam = &cameras[1];
		//cout << "Camera 1 Side View Camera \n";
		//cout << "camera 1 /n" << cameras[1].getDistance();
		break;
	case 2:
		focusCam = &cameras[2];
	//	cout << "Camera 2 On Board Terrain Camera \n";
		break;
	case 3:
		focusCam = &cameras[3];
	//	cout << "Camera 3 \n";
		break;
	case 4:
		focusCam = &cameras[4];
		//cout << "Camera 4 \n";
		break;
	case 5:
		focusCam = &cameras[5];
		//cout << "Camera 5 \n";
		break;
	default:
		focusCam = &cameras[key];
	}
}
Cameras::~Cameras()
{
}
