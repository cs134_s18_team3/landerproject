#pragma once

#include "ofMain.h"
#include "Cameras.h"
#include "ofxAssimpModelLoader.h"
#include "box.h"
#include "ray.h"
#include "ParticleSystem.h"
#include "ParticleEmitter.h"

#include "Entity.h"
#include "InputManager.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		void initLightingAndMaterials();

		void initLightingAndMaterialsFlat();

		//APPLICATION FUNCTIONS AND ATTRIBUTES BELOW HERE
		Cameras cameraManager;
		int cameraCounter;
		ofPoint focus;
		//ofxAssimpModelLoader terrain;

		// Octree Methods
		void indexVertices(Box & leaf, vector <int> & supersetIndices, ofMesh & mesh);
		void getTraceBoxes(Box & searchBox, Ray & searchRay, vector <Box> & collection, int maxLevel, int currentLevel);
		ofVec3f getPointFromTrace(Box & searchBox, Ray & raytrace, ofMesh & mesh, int maxLevel, int currentLevel);
		//ofVec3f getPointFromPoint(Box & searchBox, ofVec3f & point, ofMesh & mesh, int maxLevel, int currentLevel);
		void recursiveBoxDraw(Box *targetBox, int maxLevel, int currentLevel);
		void populateLeaves(Box & root, int level, int maxLevel, ofMesh & mesh);
		bool doTargetedPointSelection(Box & searchBox, ofVec3f & resultPoint, Ray & raytrace, ofMesh & mesh); // Ray Version
		bool doTargetedPointSelection(Box & searchBox, ofVec3f & resultPoint, ofMesh & mesh); // Mouse Version
		void drawBox(const Box &box);
		Box meshBounds(const ofMesh &);
		void subDivideBox8(const Box &b, vector<Box> & boxList);

		// Lander Project Methods
		void landerMovement();
		void playLanderSound();
		void checkLanderTerrainCollision();
		
		// Util Variables
		ofSoundPlayer soundPlayer;
		bool mousePosSelected;
		ofVec3f mousePos;
		bool soundFileLoaded = false;
		bool toggleFocus = false;
		bool isFullScreen = false;
		const float selectionRange = 4.0;
		const float collisionTerrainDist = 2.0;
		const float restitution = 1.5;

		// Lander Variables
		ofMesh terrainMesh;
		Box boundingBox;
		ofxAssimpModelLoader terrain, lander;
		ParticleSystem sysLander; // contains single lander particle
		ThrustForce *thrustForce;
		ImpulseForce *impulseForce;
		GravityForce *gravityForce;
		TurbulenceForce *turbulenceForce;
		ofVec3f thrustVector;
		ParticleEmitter *thrustEmitter1,  *explosionEmitter;
		ofSoundPlayer landerSound;
		ofVec3f landerPos, landerRayDir, octreeSelectedPoint;
		Ray landerRay;

		bool bPointSelected;
		bool bCollision;
		
		ofImage bckgnd; 
		//Entity List
		Entity starFoxArwing;
		//Missile playerMissile = Missile(true);
		Entity cockPit;
		vector<Entity*> entContainer; 
		InputManager inputs;

		void setupScene();
		void loadVbo();

		// textures
		//
		ofTexture  particleTex;

		// shaders
		//
		ofVbo vbo;
		ofShader shader;
};
