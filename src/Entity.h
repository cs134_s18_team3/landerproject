#pragma once
#include "ofMain.h"
#include "ofxAssimpModelLoader.h"
#include "box.h"
class Entity
{
public:
	//Attributes
	ofVec3f pos;
	ofxAssimpModelLoader model;
	ofMesh mesh;
	bool modelLoaded = false;
	bool shouldDraw = true;
	bool drawWireFrame = true;
	float boundingSphereRadius;
	ofVec3f boundingBoxDimensions = ofVec3f(.1,.1,.1);
	float yawOffset = 0;
	float angPitch = 0;
	float angYaw = 0;
	float angRoll = 0;
	ofVec3f scale;
	ofColor color = ofColor::white;
	ofMaterial material;
	//Entity * parent;


	//Functionality
	Entity();
	Entity(string modelName, ofVec3f scale);
	~Entity();

	void loadModel(string filename);
	Box getBoundingBox();
	virtual void draw();

	virtual void update();
	void setAngle(ofVec3f ang);
	ofVec3f getAngle();
	ofVec3f getDir();


};


class Missile : public Entity {
public:
	float expireTime = 0;
	bool playerMissile = false;
	Missile(bool team);
	virtual void update() override;
	virtual void draw() override;

};

class Arwing : public Entity { //Development suspended due to time constraints
public:
	ofVec3f velocity;
	Arwing();
	void handleMovement();
	void update() override;
};