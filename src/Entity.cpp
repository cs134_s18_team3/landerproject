#include "Entity.h"


Entity::Entity()
{
	pos = ofVec3f(0, 0, 0);
}
Entity::Entity(string modelName, ofVec3f setScale) {
	pos = ofVec3f(0, 0, 0);
	scale = setScale;
	loadModel(modelName);
}

Entity::~Entity()
{
}

void Entity::loadModel(string filename) {
	model.loadModel(filename);
	model.setScale(scale.x, scale.y, scale.z);
	mesh = model.getMesh(0);
	//model.disableMaterials();
	modelLoaded = true;
}

Box Entity::getBoundingBox() {
	if (!modelLoaded) return Box();
	ofVec3f max = boundingBoxDimensions;
	ofVec3f min = -boundingBoxDimensions;
	//max.rotate(angYaw, pos, ofVec3f(0, 1, 0)); min.rotate(angYaw, pos, ofVec3f(0, 1, 0));
	max += pos; min += pos;

	return Box(Vector3(min.x, min.y, min.z), Vector3(max.x, max.y, max.z));
}

void Entity::draw() {
	if (!shouldDraw) return;
	model.enableColors();
	//ofSetColor(color);
	model.drawFaces();
	if (!drawWireFrame) return;
	/*
	ofVec3f scalePop = scale;
	model.setScale(scale.x * 1.1, scale.y * 1.1, scale.z * 1.1);
	ofSetLineWidth(5);
	ofSetColor(ofColor::white);
	model.drawWireframe();
	model.setScale(scalePop.x, scalePop.y, scalePop.z);
	*/
}

void Entity::update() {
	//cout << shouldDraw;
	model.setPosition(pos.x, pos.y, pos.z);
	model.setRotation(1, angPitch, 1, 0, 0);
	model.setRotation(0, angYaw + yawOffset, 0,1,0);
	model.setRotation(2, angRoll, 0, 0, 1);
	//model.setRotation(0, angPitch, 1, 0, 0);
	//model.setRotation(0, angRoll, 0, 0, 1);
}

void Entity::setAngle(ofVec3f ang) {
	angPitch = ang.x;
	angYaw = ang.y;
	angRoll = ang.z;
}
ofVec3f Entity::getAngle()
{
	return ofVec3f(angPitch, angYaw, angRoll);
}

ofVec3f Entity::getDir() {
	ofVec3f vecDir = ofVec3f(0,0,1);
	vecDir.rotate(angPitch, ofVec3f(1, 0, 0));
	vecDir.rotate(angYaw, ofVec3f(0, 1, 0));
	//vecDir.rotate(angRoll, ofVec3f(0, 0, 1));
	return vecDir;
}

Missile::Missile(bool isPlayerMissile)
{
	Entity::Entity();
	scale = ofVec3f(.015, .015, .015);
	loadModel("geo/Missile.obj");
	boundingBoxDimensions = ofVec3f(1, 1, 1);
	playerMissile = isPlayerMissile;
	expireTime = ofGetElapsedTimef() + 6;
}

void Missile::update()
{
	if (ofGetElapsedTimef() > expireTime) { return; }
	pos += getDir();
	//cout << pos + getDir()*(2 * (1 / ofGetFrameRate())) << "\n";
	angRoll += 10;
	//cout << "I'm here!\n";
	Entity::update();
}
void Missile::draw() {
	if (ofGetElapsedTimef() > expireTime) { return; }
	Entity::draw();
}

Arwing::Arwing()
{
	Entity::Entity();
	scale = ofVec3f(.05);
	loadModel("geo/ArwingHiPoly.obj");
	boundingBoxDimensions = ofVec3f(4, 2, 4);
}

void Arwing::update() {
	handleMovement();
}

void Arwing::handleMovement() {
	if (ofGetKeyPressed(OF_KEY_UP)) {

	}
	if (ofGetKeyPressed(OF_KEY_DOWN)) {

	}
	if (ofGetKeyPressed(OF_KEY_LEFT)) {
		angYaw += 1;
	}
	if (ofGetKeyPressed(OF_KEY_RIGHT)) {
		angYaw -= 1;
	}
	if (ofGetKeyPressed(GLFW_KEY_SPACE)) {

	}
	if (ofGetKeyPressed(GLFW_KEY_LEFT_SHIFT)) {

	}
}
