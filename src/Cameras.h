#pragma once
#include "ofMain.h"
#include <vector>
//Code Written by Jason Liu
// Camera System to organize and control camera functionality
class Cameras
{
public:
	void setup();
	void begin();
	void end();
	void setPosition(ofVec3f p, int k);
	void switchCamera(int key);

	//void update();

	Cameras();
	~Cameras();
	ofEasyCam  cam0, cam1, cam2, cam3, cam4, cam5, cam6;
	ofEasyCam *focusCam;
	std::vector<ofEasyCam> cameras = { cam0, cam1, cam2, cam3, cam4, cam5, cam6 };
};