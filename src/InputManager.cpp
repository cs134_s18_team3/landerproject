#include "InputManager.h"
//
//Input Manager
//
void InputManager::handleInputEnter(int key) {
	InputState[key] = true;
}
void InputManager::handleInputExit(int key) {
	InputState[key] = false;
}
void InputManager::handleMousePress(int butt) {
	MouseState[butt] = true;
}
void InputManager::handleMouseDepress(int butt) {
	MouseState[butt] = false;
}
bool InputManager::keyIsPressed(int key) {
	return InputState[key];
}
bool InputManager::mouseIsPressed(int button) {
	return MouseState[button];
}